package com.navoichyk.weatherappkotlin

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.navoichyk.weatherappkotlin.weatherlist.WeatherListFragment
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment(private var units: String) : Fragment() {
    private var viewsActivityInterface: ViewsActivityInterface? = null
    private var weatherListFragment: WeatherListFragment? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ViewsActivityInterface) {
            viewsActivityInterface = context
        }
        weatherListFragment = WeatherListFragment.newInstance()
    }

    override fun onDetach() {
        super.onDetach()
        viewsActivityInterface = null
        weatherListFragment = null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_settings, container, false)

    override fun onResume() {
        super.onResume()
        if (units == metric) {
            metricRadioButton.isChecked = true
        }
        if (units == imperial) {
            imperialRadioButton.isChecked = true
        }

        settingsGroup.setOnCheckedChangeListener { settingsGroup, i ->
            when (i) {
                R.id.metricRadioButton -> {
                    units = metric
                }
                R.id.imperialRadioButton -> {
                    units = imperial
                }
            }
        }

        saveSettingsButton.setOnClickListener {
            weatherListFragment?.setUnits(units)
            viewsActivityInterface?.setUnits(units)
            activity!!.onBackPressed()
        }
    }

    companion object {
        const val TAG = "SettingsFragment"
        fun newInstance(units: String) = SettingsFragment(units)
    }
}