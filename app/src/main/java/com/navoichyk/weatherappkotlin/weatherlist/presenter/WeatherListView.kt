package com.navoichyk.weatherappkotlin.weatherlist.presenter

import com.navoichyk.weatherappkotlin.repository.WeatherDataModel

interface WeatherListView {
    fun showWeatherList(list: List<WeatherDataModel>)
    fun setCityName(getCityName: String)
    fun setUnits(getUnits: String)
}