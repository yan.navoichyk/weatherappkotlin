package com.navoichyk.weatherappkotlin.weatherlist

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.navoichyk.weatherappkotlin.*
import com.navoichyk.weatherappkotlin.repository.WeatherDataModel
import com.navoichyk.weatherappkotlin.weatherlist.presenter.WeatherListPresenter
import com.navoichyk.weatherappkotlin.weatherlist.presenter.WeatherListPresenterImpl
import com.navoichyk.weatherappkotlin.weatherlist.presenter.WeatherListView
import kotlinx.android.synthetic.main.fragment_weather_list.*

class WeatherListFragment() : Fragment(),
    WeatherListView {
    private var viewsActivityInterface: ViewsActivityInterface? = null
    private var presenter: WeatherListPresenter? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ViewsActivityInterface) {
            viewsActivityInterface = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        viewsActivityInterface = null
        presenter?.dispose()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_weather_list, container, false)

    override fun onResume() {
        super.onResume()
        initWeatherList()
        presenter =
            WeatherListPresenterImpl(
                this,
                viewsActivityInterface,
                units
            )
        presenter?.fetchWeatherList(cityName)
    }


    private fun initWeatherList() {
        viewWeatherList.apply {
            adapter = WeatherAdapter()
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        }
    }

    override fun showWeatherList(list: List<WeatherDataModel>) {
        (viewWeatherList.adapter as WeatherAdapter).updateItemList(list)
        textCity.text = list[0].city
        textMainTemp.text = list[0].temp
        textMainDescription.text = list[0].description
    }

    override fun setCityName(getCityName: String) {
        cityName = getCityName
    }

    override fun setUnits(getUnits: String) {
        units = getUnits
    }

    companion object {
        const val TAG = "WeatherListFragment"
        var cityName = "Minsk"
        var units = metric
        fun newInstance() =
            WeatherListFragment()
    }

}