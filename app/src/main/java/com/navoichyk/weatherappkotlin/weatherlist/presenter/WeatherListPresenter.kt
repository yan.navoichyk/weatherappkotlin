package com.navoichyk.weatherappkotlin.weatherlist.presenter

interface WeatherListPresenter {
    fun fetchWeatherList(cityName: String)
    fun dispose()
}