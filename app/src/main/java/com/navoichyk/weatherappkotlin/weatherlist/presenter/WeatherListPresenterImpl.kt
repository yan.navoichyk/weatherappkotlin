package com.navoichyk.weatherappkotlin.weatherlist.presenter

import android.util.Log
import android.view.View
import com.navoichyk.weatherappkotlin.ViewsActivityInterface
import com.navoichyk.weatherappkotlin.repository.getWeatherList
import io.reactivex.disposables.Disposable

class WeatherListPresenterImpl(
    private var weatherListView: WeatherListView?,
    private var viewsActivityInterface: ViewsActivityInterface?,
    private val units: String
) : WeatherListPresenter {
    private var disposable: Disposable? = null
    override fun fetchWeatherList(cityName: String) {
        viewsActivityInterface!!.setProgressBar(View.VISIBLE)
        disposable = getWeatherList(
            cityName,
            units
        )
            .subscribe({ list ->
                weatherListView!!.showWeatherList(list)
                viewsActivityInterface!!.setProgressBar(View.INVISIBLE)
            }, { throwable ->
                Log.d("ERROR ", throwable.toString())
                viewsActivityInterface!!.setProgressBar(View.INVISIBLE)
            })
    }

    override fun dispose() {
        disposable?.dispose()
        weatherListView = null
    }

}