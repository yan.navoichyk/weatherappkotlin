package com.navoichyk.weatherappkotlin.weatherlist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.navoichyk.weatherappkotlin.R
import com.navoichyk.weatherappkotlin.repository.WeatherDataModel
import kotlinx.android.synthetic.main.item_weather.view.*

class WeatherAdapter() : RecyclerView.Adapter<WeatherAdapter.WeatherItemViewHolder>() {
    private val itemList = mutableListOf<WeatherDataModel>()

    class WeatherItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(weatherDataModel: WeatherDataModel) {
            with(weatherDataModel) {
                itemView.apply {
                    textTime.text = date
                    textDescription.text = description
                    Glide.with(itemView.context)
                        .load(icon)
                        .into(itemView.image_weather)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        WeatherItemViewHolder(
            itemView = parent.run {
                LayoutInflater.from(context).inflate(
                    R.layout.item_weather,
                    this,
                    false
                )
            })

    override fun getItemCount() = itemList.size

    override fun onBindViewHolder(holder: WeatherItemViewHolder, position: Int) {
        holder.bind(itemList[position])
    }

    fun updateItemList(itemListIn: List<WeatherDataModel>) {
        itemList.apply {
            clear()
            addAll(itemListIn)
        }
        notifyDataSetChanged()
    }
}