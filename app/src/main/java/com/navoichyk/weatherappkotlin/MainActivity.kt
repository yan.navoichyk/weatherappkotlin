package com.navoichyk.weatherappkotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Context
import com.navoichyk.weatherappkotlin.citieslist.AddCityFragment
import com.navoichyk.weatherappkotlin.weatherlist.WeatherListFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ViewsActivityInterface {
    private var units = metric

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getUnits()
        supportFragmentManager
            .beginTransaction()
            .add(R.id.fragment, WeatherListFragment.newInstance(), WeatherListFragment.TAG)
            .commit()
        imageAddCityButton.setOnClickListener {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment, AddCityFragment.newInstance(), AddCityFragment.TAG)
                .addToBackStack(AddCityFragment.TAG)
                .commit()
        }
        imageSettingsButton.setOnClickListener {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment, SettingsFragment.newInstance(units), SettingsFragment.TAG)
                .addToBackStack(SettingsFragment.TAG)
                .commit()
        }
    }

    private fun getUnits() {
        val sharePreferences = getSharedPreferences(settings, Context.MODE_PRIVATE)
        if (sharePreferences.contains(settings)) {
            if (sharePreferences.getString(settings, null) == metric) {
                units = metric
            }
            if (sharePreferences.getString(settings, null) == imperial) {
                units = imperial
            }
        }
    }

    override fun setProgressBar(view: Int) {
        progressBar.visibility = view
    }

    override fun setUnits(getUnits: String) {
        val sharePreferences = getSharedPreferences(settings, Context.MODE_PRIVATE)
        val edit = sharePreferences.edit()
        edit.putString(settings, getUnits)
        edit.apply()
    }
}
