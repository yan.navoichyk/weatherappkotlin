package com.navoichyk.weatherappkotlin.citieslist

import com.navoichyk.weatherappkotlin.db.CityEntity

interface CitiesListView {
    fun showCitiesList(list: List<CityEntity>)
}