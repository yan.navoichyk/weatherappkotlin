package com.navoichyk.weatherappkotlin.citieslist

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.navoichyk.weatherappkotlin.*
import com.navoichyk.weatherappkotlin.citieslist.presenter.CitiesListPresenter
import com.navoichyk.weatherappkotlin.citieslist.presenter.CitiesListPresenterImpl
import com.navoichyk.weatherappkotlin.db.CityDao
import com.navoichyk.weatherappkotlin.db.CityDatabase
import com.navoichyk.weatherappkotlin.db.CityEntity
import com.navoichyk.weatherappkotlin.weatherlist.WeatherListFragment
import kotlinx.android.synthetic.main.fragment_add_city.*

class AddCityFragment : Fragment(),
    CitiesListView,
    CitiesListAdapter.TouchClickListener {
    private var presenter: CitiesListPresenter? = null
    private var db: CityDatabase? = null
    private var dbDao: CityDao? = null
    private var listCities = mutableListOf<CityEntity>()
    private var viewsActivityInterface: ViewsActivityInterface? = null
    private var weatherListFragment: WeatherListFragment? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ViewsActivityInterface) {
            viewsActivityInterface = context
        }
        weatherListFragment =
            WeatherListFragment.newInstance()
    }

    override fun onResume() {
        super.onResume()
        db = CityDatabase.getDatabase(context!!)
        dbDao = db?.cityDao()
        initCitiesList()
        presenter =
            CitiesListPresenterImpl(
                this,
                dbDao
            )
        presenter?.fetchCitiesList()
        saveCityButton.setOnClickListener {
            (presenter as CitiesListPresenterImpl).saveCity(listCities, addCityText.text.toString())
            addCityText.text.clear()
        }
    }

    override fun onDetach() {
        super.onDetach()
        viewsActivityInterface = null
        weatherListFragment = null
        presenter?.dispose()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_add_city, container, false)

    private fun initCitiesList() {
        viewCitiesList.apply {
            adapter =
                CitiesListAdapter(this@AddCityFragment)
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        }
    }

    companion object {
        const val TAG = "AddCityFragment"
        fun newInstance() =
            AddCityFragment()
    }

    override fun showCitiesList(list: List<CityEntity>) {
        listCities = list as MutableList<CityEntity>
        (viewCitiesList.adapter as? CitiesListAdapter)?.updateItemList(list)
    }

    override fun onClickAdapter(position: Int, cityEntity: CityEntity?) {
        weatherListFragment!!.setCityName(cityEntity!!.city)
        activity!!.onBackPressed()
    }
}