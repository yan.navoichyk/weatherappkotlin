package com.navoichyk.weatherappkotlin.citieslist.presenter

import com.navoichyk.weatherappkotlin.citieslist.CitiesListView
import com.navoichyk.weatherappkotlin.db.CityDao
import com.navoichyk.weatherappkotlin.db.CityEntity
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class CitiesListPresenterImpl(
    private var citiesListView: CitiesListView?,
    private var dbDao: CityDao?
) : CitiesListPresenter {
    private val disposable: Disposable by lazy { getCitiesFromDatabase() }

    override fun fetchCitiesList() {
        disposable
    }

    override fun dispose() {
        disposable.dispose()
    }

    override fun saveCity(listCities: MutableList<CityEntity>, textCity: String) {
        if (listCities.isNotEmpty()) {
            var isExist = false
            for (i in listCities) {
                if (i.city == textCity) {
                    isExist = true
                }
            }
            if (!isExist) {
                dbDao!!.insert(CityEntity(city = textCity))
            }
        } else dbDao!!.insert(CityEntity(city = textCity))
        getCitiesFromDatabase()
    }

    private fun getCitiesFromDatabase(): Disposable {
        return Single.create<MutableList<CityEntity>> { emitter ->
            if (dbDao!!.getAll().size > 0) {
                emitter.onSuccess(dbDao!!.getAll() as MutableList<CityEntity>)
            }
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { list ->
                citiesListView?.showCitiesList(list)
            }
    }

}