package com.navoichyk.weatherappkotlin.citieslist.presenter

import com.navoichyk.weatherappkotlin.db.CityEntity

interface CitiesListPresenter {
    fun fetchCitiesList()
    fun dispose()
    fun saveCity(listCities: MutableList<CityEntity>, textCity: String)
}