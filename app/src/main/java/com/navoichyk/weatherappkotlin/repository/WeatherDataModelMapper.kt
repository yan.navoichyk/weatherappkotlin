package com.navoichyk.weatherappkotlin.repository

import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class WeatherDataModelMapper : (String) -> List<WeatherDataModel> {

    override fun invoke(jsonData: String): List<WeatherDataModel> {
        val jsonObject = JSONObject(jsonData)
        val jsonCityArray = jsonObject.getJSONObject("city")
        val city = jsonCityArray.getString("name")
        val jsonListArray = jsonObject.getJSONArray("list")
        val itemList = mutableListOf<WeatherDataModel>()
        if (jsonListArray.length() != 0) {
            for (i in 0..10) {
                val jsonWeatherArray = jsonListArray.getJSONObject(i).getJSONArray("weather")
                val dateText = (parseDate(
                    jsonListArray.getJSONObject(i).getString("dt_txt"),
                    "yy-mm-dd HH:mm:ss"
                )?.hours.toString()) + ":00"
                val temp = jsonListArray.getJSONObject(i).getJSONObject("main").getString("temp")
                for (index in 0 until jsonWeatherArray.length()) {
                    val dataModel = with(jsonWeatherArray.getJSONObject(index)) {
                        WeatherDataModel(
                            main = getString("main"),
                            description = getString("description"),
                            icon = "https://openweathermap.org/img/wn/" + getString("icon") + ".png",
                            id = getString("id"),
                            temp = temp,
                            city = city,
                            date = dateText
                        )
                    }
                    itemList.add(dataModel)
                }
            }
            return itemList
        }
        return emptyList()
    }
}

@Throws(ParseException::class)
fun parseDate(date: String, format: String): Date? {
    val formatter = SimpleDateFormat(format)
    return formatter.parse(date)
}