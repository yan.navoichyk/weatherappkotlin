package com.navoichyk.weatherappkotlin.repository

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.ResponseBody

fun getWeatherList(cityName: String, units: String): Single<List<WeatherDataModel>> {
    val newsDataModelMapper: (String) -> List<WeatherDataModel> =
        WeatherDataModelMapper()
    val url =
        "https://api.openweathermap.org/data/2.5/forecast?q=$cityName&units=$units&appid=f14ff915bd7010b691e3d72c563e3a45"
    val request = Request.Builder()
        .url(url)
        .build()
    val okHttp = OkHttpClient()
    return Single.create<String> { emitter ->
        okHttp.newCall(request).execute().use { response ->
            if (!response.isSuccessful) {
                emitter.onError(Throwable("Server error code: ${response.code}"))
            }
            if (response.body == null) emitter.onError(NullPointerException("Body is null"))
            emitter.onSuccess((response.body as ResponseBody).string())
        }
    }.subscribeOn(Schedulers.io())
        .map { jsonData -> newsDataModelMapper(jsonData) }
        .observeOn(AndroidSchedulers.mainThread())

}