package com.navoichyk.weatherappkotlin


interface ViewsActivityInterface {
    fun setProgressBar(view: Int)
    fun setUnits(getUnits: String)
}