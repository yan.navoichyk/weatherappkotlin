package com.navoichyk.weatherappkotlin

const val settings = "settings"
const val metric = "metric"
const val imperial = "imperial"
const val cityName = "cityName"